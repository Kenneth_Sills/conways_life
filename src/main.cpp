#include <string>
#include <exception>
#include <iostream>

#include "Help.h"
#include "World.h"
#include "Game_Config.h"

/*
 * Disclaimer - this version does not attempt nor claim to be blazing fast.
 * This is made for correctness, readability, ease of extension, and demonstration of knowledge,
 * whilst being *sufficiently* fast. 
 * It is also incredibly over-engineered for the problem.
 */

int main(int argc, char **argv) {
	Game_Config config;
	
	// Parse command line options.
	bool invalid_args = false;
	for (auto i = 1; i < argc; i++) {
		auto arg = std::string(argv[i]);
		
		if (arg ==  "-w" || arg ==  "--width") {
			auto width_string = std::string(argv[++i]);
			try {
				config.width = std::stoi(width_string);
			} catch (std::exception& e) {
				std::cout << "Invalid width: " << width_string << '\n';
				invalid_args = true;
			}
		} else if (arg ==  "-h" || arg ==  "--height") {
			auto height_string = std::string(argv[++i]);
			try {
				config.height = std::stoi(height_string);
			} catch (std::exception& e) {
				std::cout << "Invalid height: " << height_string << '\n';
				invalid_args = true;
			}
		} else if (arg == "-r" ||  arg ==  "--wrapping") {
			config.wrapping = true;
		} else if (arg == "-g" || arg == "--generations") {
			auto generations_string = std::string(argv[++i]);
			try {
				config.generations = std::stoi(generations_string);
			} catch (std::exception& e) {
				std::cout << "Invalid generation number: " << generations_string << '\n';
				invalid_args = true;
			}
		} else if (arg == "-i" || arg == "--input") {
			config.input_filename == std::string(argv[++i]);
		} else if (arg == "-o" || arg == "--output") {
			config.output_filename == std::string(argv[++i]);
		} else if (arg == "--help") {
			std::cout << help_text;
			return 0;
		} else {
			std::cout << "Invalid option: " << arg << '\n';
			invalid_args = true;
		}
	}
	
	if (config.width < 3 || config.height < 3) {
		std::cout << "Grid is too small. Must be at least 3x3." << '\n';
		invalid_args = true;
	}
	
	if (invalid_args) {
		return 1;
	}
}
