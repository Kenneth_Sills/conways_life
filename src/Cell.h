#pragma once

#include "World.h"

enum class Cell_State {
	Dead, 
	Alive
};

class Cell {
public:
	auto step()           -> void;
	auto status()         -> Cell_State;
	
	auto add_neighbor()    -> void;
	auto remove_neighbor() -> void;
	auto neighbor_count()  -> int;

private:
	char _neighbors;
	Cell_State _status;
};