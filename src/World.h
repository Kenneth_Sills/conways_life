#pragma once

#include <forward_list>
#include <vector>

#include "Cell.h"
#include "Game_Config.h"

struct Point {
	int x;
	int y;
};

class World {
public:
	World(Game_Config config);

	auto step()          -> void;
	auto step(int steps) -> void;
  
	auto get(int x, int y)           -> Cell*;
	auto inform_cell_change(Point p) -> void;
	
	auto Save(std::string filename) -> void;
	auto Load(std::string filename) -> void;

private:
	Game_Config _config;
	int _generation;
	std::forward_list<Point> _changed_cell_list;
	std::vector<std::vector<Cell>> _grid;
};