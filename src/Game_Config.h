#pragma once

struct Game_Config {
	std::string input_filename  = "";
	std::string output_filename = "";
	
	bool wrapping   = false;
	int width       = 10;
	int height      = 10;
	int generations = -1;
};