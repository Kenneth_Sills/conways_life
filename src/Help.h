#pragma once

#include <string>

const std::string help_text =
	"Usage: conways_life [OPTION]... [OPTION PARAM]...\n"
	"Simulates Conway's Game of Life.\n"
	"\n"
	"  -i, --input        Reads grid from designated file.\n"
	"  -o, --output       Writes end grid to designated file.\n"
	"  -w, --width        Width of the game grid.\n"
	"                     Must be at least 3.\n"
	"  -h, --height       Height of the game grid.\n"
	"                     Must be at least 3.\n"
	"  -r, --wrapping     Enable grid wrapping.\n"
	"  -g, --generations  Specify how many generations to simulate.\n"
	"                     '-1' runs infinite generations.\n"
	"      --help         Displays this help and exits.\n"
	"\n"
	"Exit status:\n"
	" 0 if OK,\n"
	" 1 if invalid command line argument.\n"
	" 2 if invalid file input.\n";