Conway's Game of Life implemented in C++.

# Disclaimer:
This version does not attempt nor claim to be blazing fast. This is made for correctness, readability, ease of extension, and demonstration of knowledge, whilst being *sufficiently* fast. 

It is also incredibly over-engineered for the problem.